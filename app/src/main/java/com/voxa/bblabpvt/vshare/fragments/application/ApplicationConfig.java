package com.voxa.bblabpvt.vshare.fragments.application;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageInfo;

import java.util.List;

public interface ApplicationConfig {
    interface View {
        void showProgress();

        void hideProgress();

        void mAllApkFiles(List<PackageInfo> packageInfo);
    }

    interface Presenter {

        void mFetchAllApplications(Context context);
    }

    interface Interactor {
        void mFetchAllApplications(FetchAllApplicationsListner listner, Context context);

        interface FetchAllApplicationsListner {
            void sucess(List<PackageInfo> packageInfo);

            void fail();
        }

    }


}
