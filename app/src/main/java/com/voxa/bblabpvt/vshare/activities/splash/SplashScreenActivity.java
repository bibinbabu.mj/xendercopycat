package com.voxa.bblabpvt.vshare.activities.splash;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.voxa.bblabpvt.vshare.R;
import com.voxa.bblabpvt.vshare.activities.home.HomePageActivity;


public class SplashScreenActivity extends AppCompatActivity {
    protected final int splash_timer = 1000;
    CountDownTimer countDownTimer = new CountDownTimer(splash_timer, 100) {
        @Override
        public void onTick(long l) {

        }

        @Override
        public void onFinish() {
            mLaunchToHomePage();
        }
    };

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        countDownTimer.start();
    }

    private void mLaunchToHomePage() {
        Intent intent = new Intent(this, HomePageActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }
}
