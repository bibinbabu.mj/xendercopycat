package com.voxa.bblabpvt.vshare.fragments.application;

import android.content.pm.PackageInfo;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.voxa.bblabpvt.vshare.R;
import com.voxa.bblabpvt.vshare.adapter.ApplicationApkAdapter;

import java.util.List;

public class SharePageFragments extends Fragment implements ApplicationConfig.View, ApplicationApkAdapter.onSelectionMutltiListner {
    private ApplicationConfig.Presenter presenter;
    private RecyclerView apkRecyclerView;
    private View rootView;
    private ActionMode actionMode;
    private ApplicationApkAdapter applicationApkAdapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new ApplicationPresenterImpl(this);
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_dashboard, container, false);

    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mInitView(view);
        mInitControls();


    }

    private void mInitControls() {
        presenter.mFetchAllApplications(getContext());
    }

    private void mInitView(View view) {
        apkRecyclerView = view.findViewById(R.id.recycle_list_view);
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void mAllApkFiles(List<PackageInfo> packageInfo) {
        applicationApkAdapter = new ApplicationApkAdapter(packageInfo, this);
        int colo = calculateNoOfColumns(102);
        apkRecyclerView.setLayoutManager(new GridLayoutManager(getContext(), colo));
        apkRecyclerView.setAdapter(applicationApkAdapter);
    }

    private int calculateNoOfColumns(float columnWidthDp) { // For example columnWidthdp=180
        DisplayMetrics displayMetrics = this.getResources().getDisplayMetrics();
        float screenWidthDp = displayMetrics.widthPixels / displayMetrics.density;
        int noOfColumns = (int) (screenWidthDp / columnWidthDp + 0.5); // +0.5 for correct rounding to int.
        return noOfColumns;
    }

    private ActionMode.Callback callback = new ActionMode.Callback() {
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            MenuInflater inflater = mode.getMenuInflater();
            inflater.inflate(R.menu.nav_menu, menu);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
                case R.id.nav_help:
//                    Toast.makeText(activity, adapter.getSelected().size() + " selected", Toast.LENGTH_SHORT).show();
                    mode.finish();
                    return true;
                default:
                    return false;
            }
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            actionMode = null;
        }
    };


    @Override
    public void mMultipleSelection(int position) {
        //int selected = adapter.getSelected().size();
        if (actionMode == null) {
            actionMode = getActivity().startActionMode(callback);
            actionMode.setTitle("Selected: ");
        } /*else {
            if (selected == 0) {
                actionMode.finish();
            } else {
                actionMode.setTitle("Selected: " + selected);
            }
        }*/
    }

    public void deselectAllApk(View v) {
      //  applicationApkAdapter.clearSelected();
        if (actionMode != null) {
            actionMode.finish();
            actionMode = null;
        }
    }
}
