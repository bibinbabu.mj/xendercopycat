package com.voxa.bblabpvt.vshare.fragments.application;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

import java.util.List;

public class ApplicationInteractorImpl implements ApplicationConfig.Interactor {

    @Override
    public void mFetchAllApplications(FetchAllApplicationsListner listner, Context context) {
        List<PackageInfo> packageInfo=context.getPackageManager().getInstalledPackages(PackageManager.GET_META_DATA);
          listner.sucess(packageInfo);
    }
}
