package com.voxa.bblabpvt.vshare.navigation;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.navigation.NavDestination;
import androidx.navigation.NavOptions;
import androidx.navigation.Navigator;
import androidx.navigation.fragment.FragmentNavigator;

@Navigator.Name("custom_navigator")
public class CustomNavigation extends FragmentNavigator {
    private final Context context;
    private final FragmentManager fragmentManager;
    private final int containerId;

    public CustomNavigation(@NonNull Context context, @NonNull FragmentManager fragmentManager, int containerId) {
        super(context, fragmentManager, containerId);
        this.context = context;
        this.fragmentManager = fragmentManager;
        this.containerId = containerId;
    }

    @Nullable
    @Override
    public NavDestination navigate(@NonNull Destination destination, @Nullable Bundle args, @Nullable NavOptions navOptions, @Nullable Navigator.Extras navigatorExtras) {
        // return super.navigate(destination, args, navOptions, navigatorExtras);
        String fragment_tag = String.valueOf(destination.getId());
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        boolean intailFragmentNavigate = false;
        Fragment currentFragment = fragmentManager.getPrimaryNavigationFragment();
        if (currentFragment != null) {
            fragmentTransaction.detach(currentFragment);
        } else intailFragmentNavigate = true;

        Fragment fragment = fragmentManager.findFragmentByTag(fragment_tag);
        if (fragment == null) {
            String fragmentClassName = destination.getClassName();
            fragment = fragmentManager.getFragmentFactory().instantiate(context.getClassLoader(), fragmentClassName);
            fragmentTransaction.add(containerId, fragment, fragment_tag);
        } else fragmentTransaction.attach(fragment);

        fragmentTransaction.setPrimaryNavigationFragment(fragment);
        fragmentTransaction.setReorderingAllowed(true);
        fragmentTransaction.commitNow();
        if (intailFragmentNavigate) {
            return destination;
        } else return null;
    }
}
