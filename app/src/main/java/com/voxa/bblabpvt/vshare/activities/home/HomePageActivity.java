package com.voxa.bblabpvt.vshare.activities.home;

import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;
import com.voxa.bblabpvt.vshare.R;
import com.voxa.bblabpvt.vshare.activities.language.LangugeActivity;
import com.voxa.bblabpvt.vshare.navigation.CustomNavigation;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;

public class HomePageActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private BottomNavigationView navView;
    private Toolbar toolbar;
    private DrawerLayout drawerLayout;
    private NavigationView mDrawerNavView;
    private ImageView mAppIcon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page);
        mInitView();
        mNavigationDrawerSetUp();
        mInitControls();
        mNavigationSetUp();
        mAppIconSetUp();
    }

    private void mAppIconSetUp() {
        Glide.with(this)
                .load(getApplicationContext().getResources().getDrawable(R.drawable.abc))
                .apply(RequestOptions.circleCropTransform())
                .apply(RequestOptions.skipMemoryCacheOf(true)) //
                .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.NONE))
                .transition(DrawableTransitionOptions.withCrossFade())
                .into(mAppIcon);

    }

    private void mNavigationDrawerSetUp() {
        setSupportActionBar(toolbar);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, 0, 0);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();
    }

    private void mInitControls() {
        mDrawerNavView.setNavigationItemSelectedListener(this);
    }

    private void mInitView() {
        navView = findViewById(R.id.nav_view);
        toolbar = findViewById(R.id.toolbar);
        drawerLayout = findViewById(R.id.drawer_layout);
        mDrawerNavView = findViewById(R.id.drawerNavView);
        // View hView =  mDrawerNavView.getHeaderView(0);
        View headerView = mDrawerNavView.inflateHeaderView(R.layout.nav_header);
        mAppIcon = headerView.findViewById(R.id.app_icon);
    }

    private void mNavigationSetUp() {
        // BottomNavigationView bottomNavigationView=findViewById(R.id.b)
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        Fragment navHostFragment = getSupportFragmentManager().findFragmentById(R.id.nav_host_fragment);
        CustomNavigation navigator = new CustomNavigation(this,
                navHostFragment.getChildFragmentManager(),
                R.id.nav_host_fragment);
        navController.getNavigatorProvider().addNavigator(navigator);
        navController.setGraph(R.navigation.navigation_bottom_graph);
        NavigationUI.setupWithNavController(navView, navController);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nav_language: {
                //mSetCheckSelected(item);
                item.setChecked(true);
                Intent intent = new Intent(this, LangugeActivity.class);
                mMoveToPages(intent);
                break;
            }
            case R.id.nav_about: {
                // mSetCheckSelected(item);
                Toast.makeText(this, "night clicked", Toast.LENGTH_SHORT).show();
                break;
            }
        }
       // item.setChecked(false);
        drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    private void mMoveToPages(Intent intent) {
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    /*private void mSetCheckSelected(MenuItem item) {
        if (item.isChecked()) item.setChecked(false);
        else item.setChecked(true);
    }*/

}
