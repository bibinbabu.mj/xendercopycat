package com.voxa.bblabpvt.vshare.fragments.application;

import android.content.Context;
import android.content.pm.PackageInfo;

import java.util.List;

public class ApplicationPresenterImpl implements ApplicationConfig.Presenter, ApplicationConfig.Interactor.FetchAllApplicationsListner {
    private ApplicationConfig.Interactor interactor;
    private ApplicationConfig.View view;

    public ApplicationPresenterImpl(ApplicationConfig.View view) {
        this.interactor = new ApplicationInteractorImpl();
        this.view = view;
    }

    @Override
    public void mFetchAllApplications(Context context) {
        interactor.mFetchAllApplications(this, context);
    }

    @Override
    public void sucess(List<PackageInfo> packageInfos) {
        if (view != null) {
            view.mAllApkFiles(packageInfos);
        }

    }

    @Override
    public void fail() {

    }
}
