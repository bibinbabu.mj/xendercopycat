package com.voxa.bblabpvt.vshare.adapter;

import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.material.card.MaterialCardView;
import com.voxa.bblabpvt.vshare.R;

import java.io.File;
import java.text.DecimalFormat;
import java.util.List;

import static java.security.AccessController.getContext;

public class ApplicationApkAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    List<PackageInfo> packageInfo;
    private PackageManager mManager;
    private onSelectionMutltiListner onSelectionMutltiListner;

    public interface onSelectionMutltiListner {
        void mMultipleSelection(int position);

    }

    public ApplicationApkAdapter(List<PackageInfo> packageInfo, onSelectionMutltiListner onSelectionMutltiListner) {
        this.packageInfo = packageInfo;
        this.onSelectionMutltiListner = onSelectionMutltiListner;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.application_apk_adapter, parent, false);
        mManager = parent.getContext().getPackageManager();
        return new ApplicationApkViewHolder(view, onSelectionMutltiListner);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ApplicationApkViewHolder) {
            ApplicationApkViewHolder apkViewHolder = (ApplicationApkViewHolder) holder;
            apkViewHolder.apkName.setText(packageInfo.get(position).applicationInfo.loadLabel(mManager));
            File file = new File(packageInfo.get(position).applicationInfo.publicSourceDir);
            double apksize = (Double.valueOf(file.length()) / (1000 * 1000));
            apkViewHolder.apkSize.setText("" + new DecimalFormat("##.##").format(apksize) + " MB");
            showApkIcon(packageInfo.get(position).applicationInfo.loadIcon(mManager), holder, apkViewHolder);

        }

        //String.valueOf(appInfo.loadLabel(mManager)
    }


    private void showApkIcon(Drawable imageICon, RecyclerView.ViewHolder holder, ApplicationApkViewHolder apkViewHolder) {
        Glide.with(holder.itemView.getContext())
                .load(imageICon)
                .apply(RequestOptions.circleCropTransform())
                .apply(RequestOptions.skipMemoryCacheOf(true)) //
                .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.NONE))
                .transition(DrawableTransitionOptions.withCrossFade())
                .into(apkViewHolder.apkIcon);


        /*GlideApp.with(getContext())
                .load(object.appInfo)
                .override(160)
                .centerCrop()
                .into(image);*/

    }

    @Override
    public int getItemCount() {
        return packageInfo.size();
    }

    private class ApplicationApkViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private final AppCompatImageView apkIcon;
        private final TextView apkName, apkSize;
        private final MaterialCardView cardView;
        private ApplicationApkAdapter.onSelectionMutltiListner onSelectionMutltiListner;
        // private final View viewBase;

        public ApplicationApkViewHolder(@NonNull View view, ApplicationApkAdapter.onSelectionMutltiListner onSelectionMutltiListner) {
            super(view);
            apkIcon = view.findViewById(R.id.apk_image);
            apkName = view.findViewById(R.id.apk_name);
            apkSize = view.findViewById(R.id.apk_size);
            cardView = view.findViewById(R.id.card);
            this.onSelectionMutltiListner = onSelectionMutltiListner;
            cardView.setOnClickListener(this);
            //  viewBase = view.findViewById(R.id.view);
        }

        @Override
        public void onClick(View v) {
            onSelectionMutltiListner.mMultipleSelection(getAdapterPosition());
        }
    }
}
